package com.techu.apihackathon.hackathon.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customers")
public class CustomersModel {
	
	@Id
	private String customerId;
	private String name;
	private String LastName;
	private String secondLastName;
	private int age;
	private String RFC;
	private String address;
	private String email;

	@Override
	public String toString() {
		return "CustomersModel{" +
				"customerId='" + customerId + '\'' +
				", name='" + name + '\'' +
				", LastName='" + LastName + '\'' +
				", secondLastName='" + secondLastName + '\'' +
				", age=" + age +
				", RFC='" + RFC + '\'' +
				", address='" + address + '\'' +
				", email='" + email + '\'' +
				'}';
	}

	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getSecondLastName() {
		return secondLastName;
	}
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getRFC() {
		return RFC;
	}
	public void setRFC(String rFC) {
		RFC = rFC;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
