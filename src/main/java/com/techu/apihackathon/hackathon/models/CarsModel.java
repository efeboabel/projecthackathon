package com.techu.apihackathon.hackathon.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cars")
public class CarsModel {

    @Id
    private String id;
    private String plates;
    private String brand;
    private String model;

    @Override
    public String toString() {
        return "CarsModel{" +
                "id='" + id + '\'' +
                ", plates='" + plates + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", use='" + use + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    private String use;
    private String status;

    public CarsModel() {
    }

    public CarsModel(String id, String plates, String brand, String model, String use, String status) {
        this.id = id;
        this.plates = plates;
        this.brand = brand;
        this.model = model;
        this.use = use;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlates() {
        return plates;
    }

    public void setPlates(String plates) {
        this.plates = plates;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
