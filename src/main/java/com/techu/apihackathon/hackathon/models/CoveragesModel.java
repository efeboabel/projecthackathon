package com.techu.apihackathon.hackathon.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "coverages")
public class CoveragesModel {

    @Id
    private String idCoverage;
    private String coverageType;
    private String coverageDescription;
    private double price;

    public String getIdCoverage() {
        return idCoverage;
    }

    @Override
    public String toString() {
        return "CoveragesModel{" +
                "idCoverage='" + idCoverage + '\'' +
                ", coverageType='" + coverageType + '\'' +
                ", coverageDescription='" + coverageDescription + '\'' +
                ", price=" + price +
                '}';
    }

    public void setIdCoverage(String idCoverage) {
        this.idCoverage = idCoverage;
    }

    public String getCoverageType() {
        return coverageType;
    }

    public void setCoverageType(String coverageType) {
        this.coverageType = coverageType;
    }

    public String getCoverageDescription() {
        return coverageDescription;
    }

    public void setCoverageDescription(String coverageDescription) {
        this.coverageDescription = coverageDescription;
    }

    public double getPrice() { return this.price; }

    public void setPrice(double price) { this.price = price; }

}
