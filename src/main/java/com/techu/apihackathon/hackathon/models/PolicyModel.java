package com.techu.apihackathon.hackathon.models;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Map;

@Document(collection = "policies")
public class PolicyModel {

    @Id
    private String id;
    private String idInsured;
    private String idCar;
    private String idCustomer;
    private Map<String, Integer> policyCoverage;
    private Date starDate;
    private Date salesDate;
    private Double totalPolicy;
    private String description;
    private String statusPolicy;

    public PolicyModel() {
    }

    public PolicyModel(String id, String idInsured, String idCar, String idCustomer, Map<String, Integer> policyCoverage, Date starDate, Date salesDate, Double totalPolicy, String description, String statusPolicy) {
        this.id = id;
        this.idInsured = idInsured;
        this.idCar = idCar;
        this.idCustomer = idCustomer;
        this.policyCoverage = policyCoverage;
        this.starDate = starDate;
        this.salesDate = salesDate;
        this.totalPolicy = totalPolicy;
        this.description = description;
        this.statusPolicy = statusPolicy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdInsured() {
        return idInsured;
    }

    public void setIdInsured(String idInsured) {
        this.idInsured = idInsured;
    }

    public String getIdCar() {
        return idCar;
    }

    public void setIdCar(String idCar) {
        this.idCar = idCar;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public Map<String, Integer> getPolicyCoverage() {
        return policyCoverage;
    }

    public void setPolicyCoverage(Map<String, Integer> policyCoverage) {
        this.policyCoverage = policyCoverage;
    }

    public Date getStarDate() {
        return starDate;
    }

    public void setStarDate(Date starDate) {
        this.starDate = starDate;
    }

    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    public Double getTotalPolicy() {
        return totalPolicy;
    }

    public void setTotalPolicy(Double totalPolicy) {
        this.totalPolicy = totalPolicy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatusPolicy() {
        return statusPolicy;
    }

    public void setStatusPolicy(String statusPolicy) {
        this.statusPolicy = statusPolicy;
    }

    @Override
    public String toString() {
        return "PolicyModel{" +
                "id='" + id + '\'' +
                ", idInsured='" + idInsured + '\'' +
                ", idCar='" + idCar + '\'' +
                ", idCustomer='" + idCustomer + '\'' +
                ", idCoverage='" + policyCoverage + '\'' +
                ", starDate=" + starDate +
                ", salesDate=" + salesDate +
                ", totalPolicy=" + totalPolicy +
                ", description='" + description + '\'' +
                ", statusPolicy='" + statusPolicy + '\'' +
                '}';
    }
}
