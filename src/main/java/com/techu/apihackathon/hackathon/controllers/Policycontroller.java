package com.techu.apihackathon.hackathon.controllers;

import com.techu.apihackathon.hackathon.models.PolicyModel;
import com.techu.apihackathon.hackathon.services.PolicyService;
import com.techu.apihackathon.hackathon.services.PolicyServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hackathon/v1")
public class Policycontroller {

    @Autowired
    PolicyService policyService ;

    @PostMapping("/policies")
    public ResponseEntity<PolicyServiceResponse> addPolicy(@RequestBody
                                                                  PolicyModel policy) {
        System.out.println("addPolicy");
        System.out.println("La id de la poliza a añadir es " + policy.getId());
        System.out.println("datos de la poliza " + policy.toString());


        PolicyServiceResponse result = this.policyService.addPolicy(policy);

        return new ResponseEntity<>(result, result.getResponseHttpStatusCode());
    }

    @GetMapping("/policies")
    public ResponseEntity<List<PolicyModel>> getPolicy() {
        System.out.println("getPolicy");

        return new ResponseEntity<>(
                this.policyService.getPolicies(),
                HttpStatus.OK
        );
    }


}
