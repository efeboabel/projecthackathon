package com.techu.apihackathon.hackathon.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techu.apihackathon.hackathon.models.CustomersModel;
import com.techu.apihackathon.hackathon.services.CustomersService;

@RestController
@RequestMapping("/hackathon/v1")
public class CustomersController {

    @Autowired
    CustomersService customersService;
    @Autowired
    AuthController authController;
    
    @GetMapping("/customers")
    public ResponseEntity<List<CustomersModel>> getCustomers(){
        System.out.println("getCustomers");

        return new ResponseEntity<>(
                this.customersService.findAll(), HttpStatus.OK
        );
    }
    
    
    @GetMapping("/customers/{id}")
    public ResponseEntity<Object> getCustomersById(@PathVariable String id){
        System.out.println("getCustomersById");
        System.out.println("El Id del customer es " + id);

        Optional<CustomersModel> result = this.customersService.findById(id);

        return new ResponseEntity<>(
            result.isPresent() ? result.get() : "customer no encontrado",
            result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
    
    @PostMapping("/customers")
    public ResponseEntity<CustomersModel> addCustomers(@RequestBody CustomersModel customer){

        System.out.println("addcustomers");

        return new ResponseEntity<>(
                this.customersService.add(customer)
                , HttpStatus.CREATED
        );
    }
    //@PreAuthorize("hasRole('GERENTE')")
    @PutMapping("/customers/{id}")
    public ResponseEntity<CustomersModel> updateCustomers(@PathVariable String id, @RequestBody CustomersModel customer){
        System.out.println("updateCustomers");

        Optional<CustomersModel> CustomersToUpdate = this.customersService.findById(id);

        if(CustomersToUpdate.isPresent()){
            System.out.println("Cliente para actualizar encontrado, actualizando");
            this.customersService.update(customer);
        }

        return new ResponseEntity<>(
        		customer
                , CustomersToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
    @PreAuthorize("hasRole('GERENTE')")
    @DeleteMapping("/customers/{id}")
    public ResponseEntity<String> deleteCustomers(@PathVariable String id){
        System.out.println("deleteCustomers");

        boolean deleteCustomers = this.customersService.delete(id);

        return new ResponseEntity<>(
        		deleteCustomers ? "Cliente borrado" : "Cliente no encontrado"
                , deleteCustomers ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
