package com.techu.apihackathon.hackathon.controllers;

import com.techu.apihackathon.hackathon.models.CoveragesModel;
import com.techu.apihackathon.hackathon.services.CoveragesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/hackathon/v1")
public class CoveragesController {

    @Autowired
    CoveragesService coveragesService;

    @GetMapping("/coverages")
    public ResponseEntity<List<CoveragesModel>> getCoverages(){
        this.coveragesService.findAll();
        return new ResponseEntity<>(this.coveragesService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/coverage")
    public ResponseEntity<CoveragesModel> createCoverage(@RequestBody CoveragesModel coveragesModel){
        return new ResponseEntity<>(this.coveragesService.create(coveragesModel), HttpStatus.CREATED);
    }

    @GetMapping("/coverage/{id}")
    public ResponseEntity<Object> getCoverages(@PathVariable String id){
        Optional<CoveragesModel> result = this.coveragesService.findById(id);
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "No se encontro el recurso",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NO_CONTENT
        );
    }
    
   // @PreAuthorize("hasRole('GERENTE')")
    @PutMapping("/coverage/{id}")
    public ResponseEntity<CoveragesModel> updateCoverage(@RequestBody CoveragesModel coveragesModel, @PathVariable String id){
        Optional<CoveragesModel> coverageUpdate = this.coveragesService.findById(id);
        if ( coverageUpdate.isPresent() ) {
            coveragesModel.setIdCoverage(id);
            this.coveragesService.update(coveragesModel);
        }
        return new ResponseEntity<>(coveragesModel, coverageUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND );
    }
   // @PreAuthorize("hasRole('GERENTE')")
    @DeleteMapping("/coverage/{id}")
    public ResponseEntity<String> deleteCoverage(@PathVariable String id){
        boolean deleteCoverage = this.coveragesService.delete(id);
        return new ResponseEntity<>(
                deleteCoverage ? "Eliminado" : "No se encontro el recurso",
                deleteCoverage ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
