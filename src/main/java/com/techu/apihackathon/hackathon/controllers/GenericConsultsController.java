package com.techu.apihackathon.hackathon.controllers;

import com.techu.apihackathon.hackathon.services.ConsultCompleteInfoPolicyResponse;
import com.techu.apihackathon.hackathon.services.ConsultCompleteInfoPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/hackathon/v1")
public class GenericConsultsController {

    @Autowired
    private ConsultCompleteInfoPolicyService consultCompleteInfoPolicyService;

    @GetMapping("/information/{idPolicy}")
    public ResponseEntity<ConsultCompleteInfoPolicyResponse> getInfoPolicy(@PathVariable String idPolicy){
        ConsultCompleteInfoPolicyResponse result = new ConsultCompleteInfoPolicyResponse();
        result = this.consultCompleteInfoPolicyService.getInfoPolicy(idPolicy);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
