package com.techu.apihackathon.hackathon.controllers;

import com.techu.apihackathon.hackathon.models.CarsModel;
import com.techu.apihackathon.hackathon.services.CarsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/hackathon/v1")
public class CarsController {

    @Autowired
    CarsService carsService;

    @PostMapping("/cars")
    public ResponseEntity<CarsModel> addCar(@RequestBody CarsModel car){
        System.out.println("addCar");
        System.out.println("La id del carro que se va a crear es " + car.getId());
        System.out.println("La placa del carro que se va a crear es " + car.getPlates());
        System.out.println("La Marca del carro es " + car.getBrand());
        System.out.println("El modelo del carro que se va a crear es " + car.getModel());
        System.out.println("El uso del carro  que se va a crear es " + car.getUse());
        System.out.println("El estado del carro que se va a crear es" + car.getStatus());


        return new ResponseEntity<>(
                this.carsService.add(car)
                , HttpStatus.CREATED
                );
    }

    @GetMapping("/cars")
    public ResponseEntity<List<CarsModel>> getCars(){
        System.out.println("getCars");

        return new ResponseEntity<>(
                this.carsService.findAll(),
                HttpStatus.OK
        );

    }

    @PreAuthorize("hasRole('GERENTE')")
    @PutMapping("/cars/{id}")
    public ResponseEntity<CarsModel> updateCar(@RequestBody CarsModel car, @PathVariable String id){
        System.out.println("updateCar");
        System.out.println("La id del carro a actualizar en paremetro URL es " + id);
        System.out.println("La id del carro a actualizar es " + car.getId());
        System.out.println("La placa del carro a actualizar es " + car.getPlates());
        System.out.println("La Marca del carro a actualizar  es " + car.getBrand());
        System.out.println("El modelo del carro a actualizar  es " + car.getModel());
        System.out.println("El uso del carro  a actualizar es " + car.getUse());
        System.out.println("El estado del carro a actualizar es" + car.getStatus());

        Optional<CarsModel> carToUpdate = this.carsService.findById(id);

        if(carToUpdate.isPresent()){
            System.out.println("Carro para actualizar encontrado, ... Actualizando");
            this.carsService.update(car);
        }


        return new ResponseEntity<>(
                car
                ,carToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

   // @PreAuthorize("hasRole('GERENTE')")
    @DeleteMapping("/cars/{id}")
    public ResponseEntity<String> deleteCar(@PathVariable String id) {
        System.out.println("deleteCar");

        boolean deleteCar = this.carsService.delete(id);

        return new ResponseEntity<>(
                deleteCar ? "Carro borrado" : "Carro no borrado",
                deleteCar ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @GetMapping("/cars/{id}")
    public ResponseEntity<Object> getCarById(@PathVariable String id) {
        System.out.println("getCarId");
        System.out.println("La id del carro a buscar es " + id);

        Optional<CarsModel> result = this.carsService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Carro no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }








}
