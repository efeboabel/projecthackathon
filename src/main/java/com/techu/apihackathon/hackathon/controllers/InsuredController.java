package com.techu.apihackathon.hackathon.controllers;

import com.techu.apihackathon.hackathon.models.InsuredModel;
import com.techu.apihackathon.hackathon.services.InsuredService;
import com.techu.apihackathon.hackathon.services.InsuredServiceResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/hackathon/v1")
public class InsuredController {

    @Autowired
    private InsuredService insuredService;

    @GetMapping("/insured")
    public ResponseEntity<List<InsuredModel>> getInsured(){
        System.out.println("getInsured");

        return new ResponseEntity<>(this.insuredService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/insured/{id}")
    public ResponseEntity<Object> getInsuredOne(@PathVariable String id){
        System.out.println("getInsuredOne");
        System.out.println("The id of insured is " + id);

        Optional<InsuredModel> result = this.insuredService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Insured not found",
                result.isPresent() ? HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/insured")
    public ResponseEntity<InsuredServiceResponse> addInsured(@RequestBody InsuredModel insured){
        System.out.println("addInsured");
        System.out.println(insured.toString());

        InsuredServiceResponse insuranceServiceResponse = this.insuredService.add(insured);
        return new ResponseEntity<>(insuranceServiceResponse, insuranceServiceResponse.getResponseHttp());
    }
    //@PreAuthorize("hasRole('GERENTE')")
    @PutMapping("/insured/{id}")
    public ResponseEntity<InsuredModel> updateInsured(@RequestBody InsuredModel insured, @PathVariable String id){
        System.out.println("updateInsured");
        System.out.println("The id of insured to update iun URL param is " + id);
        System.out.println(insured.toString());

        Optional<InsuredModel> insuredUpdate = this.insuredService.findById(id);

        if ( insuredUpdate.isPresent() ) {
            System.out.println("Insured to update found, updated");
            insured.setId(id);
            this.insuredService.update(insured);
        }

        return new ResponseEntity<>(insured, insuredUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND );
    }
   // @PreAuthorize("hasRole('GERENTE')")
    @DeleteMapping("/insured/{id}")
    public ResponseEntity<String> deleteInsured(@PathVariable String id){
        System.out.println("deleteInsured");
        System.out.println("The id of insured is " + id);

        boolean deleteInsured = this.insuredService.delete(id);

        return new ResponseEntity<>(deleteInsured ? "Insured deleted" : "Insured not deleted", deleteInsured ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

}
