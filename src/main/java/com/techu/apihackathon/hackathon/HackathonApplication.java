package com.techu.apihackathon.hackathon;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

import com.techu.apihackathon.hackathon.models.ERoles;
import com.techu.apihackathon.hackathon.models.RoleModel;
import com.techu.apihackathon.hackathon.repositories.RoleRepository;

@SpringBootApplication
@EnableGlobalMethodSecurity (securedEnabled = true, prePostEnabled = true)
public class HackathonApplication {

	public static void main(String[] args) {
		SpringApplication.run(HackathonApplication.class, args);
	}
	 @Bean
	    CommandLineRunner init(RoleRepository roleRepository) {

	        return args -> {

	            RoleModel adminRole = roleRepository.findByRole(ERoles.GERENTE);
	            if (adminRole == null) {
	            	RoleModel newAdminRole = new RoleModel();
	                newAdminRole.setRole("GERENTE");
	                roleRepository.save(newAdminRole);
	            }

	            RoleModel userRole = roleRepository.findByRole(ERoles.PROMOTOR_A);
	            if (userRole == null) {
	            	RoleModel newUserRole = new RoleModel();
	                newUserRole.setRole("PROMOTOR_A");
	                roleRepository.save(newUserRole);
	            }
	        };

	    }

}
