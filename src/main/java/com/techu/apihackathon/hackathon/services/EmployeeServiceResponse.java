package com.techu.apihackathon.hackathon.services;

import com.techu.apihackathon.hackathon.models.EmployeeModel;
import org.springframework.http.HttpStatus;

public class EmployeeServiceResponse {
    private String msg;
    private EmployeeModel employeeModel;
    private HttpStatus responseHttpStatusCode;

    public EmployeeServiceResponse() {
    }

    public EmployeeServiceResponse(String msg, EmployeeModel employeeModel, HttpStatus responseHttpStatusCode) {
        this.msg = msg;
        this.employeeModel = employeeModel;
        this.responseHttpStatusCode = responseHttpStatusCode;
    }

    public EmployeeModel getEmployeeModel() {
        return employeeModel;
    }

    public void setEmployeeModel(EmployeeModel employeeModel) {
        this.employeeModel = employeeModel;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}
