package com.techu.apihackathon.hackathon.services;


import com.techu.apihackathon.hackathon.models.PolicyModel;
import org.springframework.http.HttpStatus;

public class PolicyServiceResponse {


    private String msg;
    private PolicyModel policy;
    private HttpStatus responseHttpStatusCode;

    public PolicyServiceResponse() {
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PolicyModel getPolicy() {
        return policy;
    }

    public void setPolicy(PolicyModel policy) {
        this.policy = policy;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}
