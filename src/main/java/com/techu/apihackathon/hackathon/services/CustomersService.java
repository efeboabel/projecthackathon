package com.techu.apihackathon.hackathon.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techu.apihackathon.hackathon.models.CustomersModel;
import com.techu.apihackathon.hackathon.repositories.CustomersRepository;

@Service
public class CustomersService {

    @Autowired
    CustomersRepository customersRepository;
    
    public List<CustomersModel> findAll() {
        System.out.println("Listado de todos los clientes");

        return this.customersRepository.findAll();
    }

    public Optional<CustomersModel> findById(String id){
        System.out.println("Encontrar cliente por Id");

        return this.customersRepository.findById(id);
    }

    public CustomersModel add(CustomersModel client){
        System.out.println("agregar cliente");

        return this.customersRepository.save(client);
    }

    public CustomersModel update(CustomersModel client){
        System.out.println("Actualizar informacion del cliente");

        return this.customersRepository.save(client);
    }

    public boolean delete(String id){
        System.out.println("Borrar cliente por id");
        boolean result = false;

        if(this.findById(id).isPresent()){
            System.out.println("Cliente encontrado, borrando");
            this.customersRepository.deleteById(id);
            result = true;
        }

        return result;
    }
    
}
