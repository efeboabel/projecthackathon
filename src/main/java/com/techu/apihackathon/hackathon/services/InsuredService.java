package com.techu.apihackathon.hackathon.services;

import com.techu.apihackathon.hackathon.models.InsuredModel;
import com.techu.apihackathon.hackathon.repositories.InsuredRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InsuredService {

    @Autowired
    private InsuredRepository insuredRepository;
    
    @Autowired
    private CustomersService customersService;

    public List<InsuredModel> findAll(){
        return this.insuredRepository.findAll();
    }

    public Optional<InsuredModel> findById(String id) {
        return this.insuredRepository.findById(id);
    }

    public InsuredServiceResponse add(InsuredModel insured) {
    	 System.out.println("add InsuredModel");

         InsuredServiceResponse result = new InsuredServiceResponse();


         result.setInsured(insured);

         if(this.customersService.findById(insured.getCustomerId()).isPresent() == false){
             System.out.println("Cliente de la poliza no se ha encontrado en el sistema");
             result.setMsg("Cliente no encontrado");
             result.setResponseHttp(HttpStatus.NOT_FOUND);
             return result;
         }
         this.insuredRepository.save(insured);
         result.setMsg("Asegurado generada con éxito ");
         result.setResponseHttp(HttpStatus.OK);
		return result;
       
    }

    public boolean delete(String id) {
        System.out.println("delete en Insured ");

        boolean result = false;

        if ( this.findById(id).isPresent() == true  ) {
            System.out.println("Producto encontrado borrado");
            this.insuredRepository.deleteById(id);
            result = true;
        }

        return result;
    }

    public InsuredModel update(InsuredModel insured){
        System.out.println("update en InsuredService");
        return this.insuredRepository.save(insured);
    }

    
}
