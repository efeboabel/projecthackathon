package com.techu.apihackathon.hackathon.services;

import com.techu.apihackathon.hackathon.models.RoleModel;
import com.techu.apihackathon.hackathon.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public RoleModel add(RoleModel role) {
        System.out.println("add RoleService");

        return this.roleRepository.save(role);
    }

    public List<RoleModel> findAll() {
        System.out.println("findAll RoleService");

        return this.roleRepository.findAll();
    }

    public Optional<RoleModel> findById(String id) {
        System.out.println("findById en RoleService");

        return this.roleRepository.findById(id);
    }

    public RoleModel update(RoleModel role) {
        System.out.println("update en RoleService");

        return this.roleRepository.save(role);
    }

    public boolean delete(String id) {
        System.out.println("delete en RoleService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Rol encontrado, Borrando");
            this.roleRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
