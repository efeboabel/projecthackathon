package com.techu.apihackathon.hackathon.services;

import com.techu.apihackathon.hackathon.models.CoveragesModel;
import com.techu.apihackathon.hackathon.repositories.CoveragesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CoveragesService {

    @Autowired
    CoveragesRepository coveragesRepository;

    public List<CoveragesModel> findAll(){
      return this.coveragesRepository.findAll();
    };

    public CoveragesModel create(CoveragesModel coveragesModel){
        this.coveragesRepository.save(coveragesModel);
        return coveragesModel;
    }

    public Optional<CoveragesModel> findById(String id){
        return this.coveragesRepository.findById(id);
    }

    public CoveragesModel update(CoveragesModel coveragesModel){
        return this.coveragesRepository.save(coveragesModel);
    }

    public boolean delete(String id) {
        boolean result = false;
        if( this.findById(id).isPresent() == true ) {
            this.coveragesRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}
