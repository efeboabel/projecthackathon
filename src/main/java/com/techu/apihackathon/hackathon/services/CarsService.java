package com.techu.apihackathon.hackathon.services;

import com.techu.apihackathon.hackathon.models.CarsModel;
import com.techu.apihackathon.hackathon.repositories.CarsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarsService {

    @Autowired
    CarsRepository carsRepository;

    public CarsModel add(CarsModel car){
        System.out.println("add CarsService");

        return this.carsRepository.save(car);
    }

    public List<CarsModel> findAll(){
        System.out.println("findAll CarsService");

        return  this.carsRepository.findAll();
    }

    public CarsModel update(CarsModel car){
        System.out.println("Update CarsService");

        return this.carsRepository.save(car);
    }

    public Optional<CarsModel> findById(String id){
        System.out.println(" findById  de CarsService");

        return this.carsRepository.findById(id);
    }

    public boolean delete(String id){
        System.out.println(" delete en CarsService");

        boolean result = false;

        if(this.findById(id).isPresent() == true){
            System.out.println("Carro encontrado borrando");
            this.carsRepository.deleteById(id);
            result = true ;

        }
        return result;

    }




}
