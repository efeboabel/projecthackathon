package com.techu.apihackathon.hackathon.services;

import com.techu.apihackathon.hackathon.models.*;
import com.techu.apihackathon.hackathon.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ConsultCompleteInfoPolicyService {

    @Autowired
    PolicyReporitory policyRepository;

    @Autowired
    InsuredRepository insuredRepository;

    @Autowired
    CarsRepository carsRepository;

    @Autowired
    CustomersRepository customersRepository;

    @Autowired
    CoveragesRepository coveragesRepository;


    public ConsultCompleteInfoPolicyResponse getInfoPolicy(String id){
        ConsultCompleteInfoPolicyResponse result = new ConsultCompleteInfoPolicyResponse();

        Optional<PolicyModel> resultPolicy;
        resultPolicy = this.policyRepository.findById(id);
        PolicyModel resultPolicyModel = resultPolicy.get();

        String idInsured = resultPolicyModel.getIdInsured();
        String idCar = resultPolicyModel.getIdCar();
        String idCustomer = resultPolicyModel.getIdCustomer();
        Map<String, Integer> coverageRep = resultPolicyModel.getPolicyCoverage();

        Optional<InsuredModel> resultInsured;
        resultInsured = this.insuredRepository.findById(idInsured);
        InsuredModel resultInsuredModel = resultInsured.get();
        result.setInsuredObject(resultInsuredModel);

        Optional<CarsModel> resultCars;
        resultCars = this.carsRepository.findById(idCar);
        CarsModel resultCarsModel = resultCars.get();
        result.setCarsObject(resultCarsModel);

        Optional<CustomersModel> resultCustomer;
        resultCustomer = this.customersRepository.findById(idCustomer);
        CustomersModel resultCustomerModel = resultCustomer.get();
        result.setCustomerObject(resultCustomerModel);

        List<Optional<CoveragesModel>> resultCoverage = new ArrayList<>();
        for (Map.Entry<String, Integer> coverageItem : coverageRep.entrySet()) {
            if(this.coveragesRepository.findById(coverageItem.getKey()).isPresent() == true){
                resultCoverage.add(this.coveragesRepository.findById(coverageItem.getKey()));
            }
        }
        result.setSalesDat(resultPolicyModel.getSalesDate());
        result.setStarDate(resultPolicyModel.getStarDate());
        result.setTotalPolicy(resultPolicyModel.getTotalPolicy().toString());
        result.setStatusPolicy(resultPolicyModel.getStatusPolicy());
        result.setPolicyNumber(resultPolicyModel.getId());
        result.setCoveragesList(resultCoverage);
        return result;
    }
}
