package com.techu.apihackathon.hackathon.services;

import com.techu.apihackathon.hackathon.models.CarsModel;
import com.techu.apihackathon.hackathon.models.CoveragesModel;
import com.techu.apihackathon.hackathon.models.CustomersModel;
import com.techu.apihackathon.hackathon.models.InsuredModel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class ConsultCompleteInfoPolicyResponse {
    String policyNumber;
    InsuredModel insuredObject;
    CustomersModel customerObject;
    List<Optional<CoveragesModel>> coveragesList;
    CarsModel CarsObject;
    String statusPolicy;
    String totalPolicy;
    Date starDate;
    Date salesDat;

    public CarsModel getCarsObject() {
        return CarsObject;
    }

    public void setCarsObject(CarsModel carsObject) {
        CarsObject = carsObject;
    }

    public CustomersModel getCustomerObject() {
        return customerObject;
    }

    public void setCustomerObject(CustomersModel customerObject) {
        this.customerObject = customerObject;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public InsuredModel getInsuredObject() {
        return insuredObject;
    }

    public void setInsuredObject(InsuredModel insuredObject) {
        this.insuredObject = insuredObject;
    }

    public List<Optional<CoveragesModel>> getCoveragesList() {
        return coveragesList;
    }

    public void setCoveragesList(List<Optional<CoveragesModel>> coveragesList) {
        this.coveragesList = coveragesList;
    }

    public String getStatusPolicy() {
        return statusPolicy;
    }

    public void setStatusPolicy(String statusPolicy) {
        this.statusPolicy = statusPolicy;
    }

    public String getTotalPolicy() {
        return totalPolicy;
    }

    public void setTotalPolicy(String totalPolicy) {
        this.totalPolicy = totalPolicy;
    }

    public Date getStarDate() {
        return starDate;
    }

    public void setStarDate(Date starDate) {
        this.starDate = starDate;
    }

    public Date getSalesDat() {
        return salesDat;
    }

    public void setSalesDat(Date salesDat) {
        this.salesDat = salesDat;
    }


}
