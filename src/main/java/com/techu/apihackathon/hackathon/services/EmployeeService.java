package com.techu.apihackathon.hackathon.services;

import com.techu.apihackathon.hackathon.models.EmployeeModel;
import com.techu.apihackathon.hackathon.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    RoleService roleService;


    public EmployeeServiceResponse add(EmployeeModel employeeModel) {
        System.out.println("add EmployeeService");

        EmployeeServiceResponse employeeServiceResponse = new EmployeeServiceResponse();
        System.out.println("El rol del empleado es: "+employeeModel.getRole());

        if (this.roleService.findById(employeeModel.getRole()).isPresent()!=true){
            System.out.println("Rol no existe");
            employeeServiceResponse.setMsg("El rol no existe");
            employeeServiceResponse.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
            return employeeServiceResponse;
        }
        if (this.employeeRepository.findById(employeeModel.getId()).isPresent()==true){
            System.out.println("El id del empleado ya existe");
            employeeServiceResponse.setMsg("El id del empleado ya existe");
            employeeServiceResponse.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
            return employeeServiceResponse;
        }

        this.employeeRepository.save(employeeModel);
        employeeServiceResponse.setMsg("Empleado agregado exitosamente");
        employeeServiceResponse.setEmployeeModel(employeeModel);
        employeeServiceResponse.setResponseHttpStatusCode(HttpStatus.CREATED);
        return employeeServiceResponse;

    }

    public List<EmployeeModel> findAll() {
    System.out.println("findAll employeeService");
    return this.employeeRepository.findAll();
    }

    public Optional<EmployeeModel> findById(String id) {
        System.out.println("findById en employeeService");
        return this.employeeRepository.findById(id);
    }

    public EmployeeModel update(EmployeeModel employeeModel) {
        System.out.println("update EmployeeService");
        return this.employeeRepository.save(employeeModel);
    }

    public boolean delete(String id) {
        System.out.println("deleteEmployee employeeService");
        boolean bandera=false;
        if (this.findById(id).isPresent()==true){
            System.out.println("Empleado eliminado");
            bandera=true;
            this.employeeRepository.deleteById(id);
        }
        return bandera;

    }
}
