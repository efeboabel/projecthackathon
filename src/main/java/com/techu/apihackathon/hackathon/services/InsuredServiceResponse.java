package com.techu.apihackathon.hackathon.services;

import org.springframework.http.HttpStatus;

import com.techu.apihackathon.hackathon.models.InsuredModel;

public class InsuredServiceResponse {

	private String msg;
	private InsuredModel insured;
	private HttpStatus responseHttp;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public InsuredModel getInsured() {
		return insured;
	}
	public void setInsured(InsuredModel insured) {
		this.insured = insured;
	}
	public HttpStatus getResponseHttp() {
		return responseHttp;
	}
	public void setResponseHttp(HttpStatus responseHttp) {
		this.responseHttp = responseHttp;
	}
	
	
}
