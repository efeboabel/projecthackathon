package com.techu.apihackathon.hackathon.services;

import com.techu.apihackathon.hackathon.models.CarsModel;
import com.techu.apihackathon.hackathon.models.PolicyModel;
import com.techu.apihackathon.hackathon.repositories.PolicyReporitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PolicyService {

    @Autowired
    PolicyReporitory policyReporitory;

    @Autowired
    CustomersService customersService;

    @Autowired
    CarsService carsService;

    @Autowired
    InsuredService insuredService;

    @Autowired
    CoveragesService coveragesService;



    public PolicyServiceResponse addPolicy(PolicyModel policy){
        System.out.println("add PolicyService");

        PolicyServiceResponse result = new PolicyServiceResponse();


        result.setPolicy(policy);

        if(this.customersService.findById(policy.getIdCustomer()).isPresent() == false){
            System.out.println("Cliente de la poliza no se ha encontrado en el sistema");
            result.setMsg("Cliente no encontrado");
            result.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);
            return result;
        }


        if(this.carsService.findById(policy.getIdCar()).isPresent() == false){
            System.out.println("Auto no se ha encontrado");
            result.setMsg("Vehiculo no encontrado en nuestro catalogo");
            result.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);
            return result;
        }


        if(this.insuredService.findById(policy.getIdInsured()).isPresent() == false){
            System.out.println("Asegurado no se ha encontrado");
            result.setMsg("Asegurado no encontrado en el sistema");
            result.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);
            return result;
        }


        double amount = 0;
        for (Map.Entry<String, Integer> coverageItem : policy.getPolicyCoverage().entrySet()) {
            if (this.coveragesService.findById(coverageItem.getKey()).isPresent() == false) {
                System.out.println("La clave de cobetura con la id " + coverageItem.getKey() + " no se encuentra en el sistema");
                result.setMsg("La cobertura con la id " + coverageItem.getKey() + " no se encuentra en el sistema");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
                return result;
            } else {
                System.out.println("Añadiendo valor de " + coverageItem.getValue() + " unidades de la clave cobertura al total");

                amount += (this.coveragesService.findById(coverageItem.getKey()).get().getPrice() * coverageItem.getValue());
                System.out.println("AMOUNT::: " + amount);
            }
        }

        policy.setTotalPolicy(amount);
        this.policyReporitory.save(policy);
        result.setMsg("Poliza generada con éxito ");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;
    }

    public List<PolicyModel> getPolicies() {
        System.out.println("getPolicies");

        return this.policyReporitory.findAll();
    }
}
