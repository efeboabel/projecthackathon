package com.techu.apihackathon.hackathon.services;

import com.techu.apihackathon.hackathon.models.RoleModel;
import com.techu.apihackathon.hackathon.models.ERoles;
import com.techu.apihackathon.hackathon.models.EmployeeModel;
import com.techu.apihackathon.hackathon.repositories.RoleRepository;
import com.techu.apihackathon.hackathon.repositories.EmployeeRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private EmployeeRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public EmployeeModel findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void saveUser(EmployeeModel user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setEnabled(true);
        RoleModel userRole = roleRepository.findByRole(ERoles.PROMOTOR_A);
        user.setRoles(new HashSet<>(Arrays.asList(userRole)));
        System.out.println("valor de user en saveUser: "+user.toString());
        userRepository.save(user);
        System.out.println("valor de user en Ya ingresado: "+user.toString());

    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

    	EmployeeModel user = userRepository.findByEmail(email);
        if (user != null) {
            List<GrantedAuthority> authorities = getUserAuthority(user.getRoles());
            return buildUserForAuthentication(user, authorities);
        } else {
            throw new UsernameNotFoundException("username not found");
        }
    }

    private List<GrantedAuthority> getUserAuthority(Set<RoleModel> userRoles) {
        Set<GrantedAuthority> roles = new HashSet<>();
        userRoles.forEach((role) -> {
            roles.add(new SimpleGrantedAuthority(role.getRole()));
        });

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles);
        return grantedAuthorities;
    }

    private UserDetails buildUserForAuthentication(EmployeeModel user, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), authorities);
    }

}
