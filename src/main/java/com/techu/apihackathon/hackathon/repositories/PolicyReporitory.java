package com.techu.apihackathon.hackathon.repositories;

import com.techu.apihackathon.hackathon.models.PolicyModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PolicyReporitory extends MongoRepository<PolicyModel, String> {
}
