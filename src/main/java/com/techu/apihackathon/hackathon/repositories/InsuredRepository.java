package com.techu.apihackathon.hackathon.repositories;

import com.techu.apihackathon.hackathon.models.InsuredModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InsuredRepository extends MongoRepository<InsuredModel, String> {
}
