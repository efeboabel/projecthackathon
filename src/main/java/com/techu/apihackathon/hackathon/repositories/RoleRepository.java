package com.techu.apihackathon.hackathon.repositories;

import com.techu.apihackathon.hackathon.models.ERoles;
import com.techu.apihackathon.hackathon.models.RoleModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends MongoRepository<RoleModel, String> {

	RoleModel findByRole(ERoles name);
}
