package com.techu.apihackathon.hackathon.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.techu.apihackathon.hackathon.models.CustomersModel;
@Repository
public interface CustomersRepository extends MongoRepository<CustomersModel, String> {

}
