package com.techu.apihackathon.hackathon.repositories;

import com.techu.apihackathon.hackathon.models.EmployeeModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends MongoRepository<EmployeeModel, String> {

	EmployeeModel findByEmail(String email);
}
