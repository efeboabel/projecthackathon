package com.techu.apihackathon.hackathon.repositories;


import com.techu.apihackathon.hackathon.models.CoveragesModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoveragesRepository extends MongoRepository<CoveragesModel, String> {

}
