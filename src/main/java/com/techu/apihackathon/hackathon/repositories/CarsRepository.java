package com.techu.apihackathon.hackathon.repositories;

import com.techu.apihackathon.hackathon.models.CarsModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CarsRepository extends MongoRepository<CarsModel,String> {

}
